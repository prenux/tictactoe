from Node import Node
import random, time


class Gametree:
    def __init__(self, entier):
        self.root = Node(None, entier, 0)

    def best_move(self, BONUS=50):
        mg = self.root.gen_metagame()
        # If root metagame is finished nothing to play
        if mg.is_finished():
            return ''
        else:
            # Create array of undefined child
            u_children, best_node = [], ''
            player, opponent = ('x', 'o') if mg.is_x_turn() else ('o', 'x')
            for c in self.gen_children(self.root):
                child_mg = c.gen_metagame()
                # If winning move in direct child return this node
                if child_mg.status == player:
                    return c
                else:
                    u_children.append(c)
                    # Give a bonus to moves that win a subgame, and initiate as best_node
                    if child_mg.squares[child_mg.lm // 9].status == player:
                        c.win_sim[player] += BONUS
                        best_node = c
                    # Penalize moves that send opponent in a finished subgame
                    if child_mg.squares[child_mg.lm % 9].is_finished():
                        c.win_sim[player] -= BONUS

            # Initialize best_node if necessary, and run monte_carlo on undef child
            best_node = best_node if best_node else u_children[0]
            return self.simulation(u_children, player, opponent, best_node)

    # Simulation and stats
    def simulation(self, u_children, player, opponent, best_node, MAXTIME=30):
        start, end = time.time(), 0
        # Move best_node at beginning of u_children so simulation will start exploring best_node first
        u_children.insert(0, u_children.pop(u_children.index(best_node)))
        # Stop if no more children or if code has been running too long
        while u_children and end - start < MAXTIME:
            n = u_children.pop(0)
            self.monte_carlo(n)
            # If node generates more wins for player, becomes the new best move
            if n.win_sim[player] > best_node.win_sim[player]:
                best_node = n
            # If node generates an equal number of wins, becomes new best move if minimizes opponent victories
            elif n.win_sim[player] == best_node.win_sim[player] and n.win_sim[opponent] < best_node.win_sim[opponent]:
                best_node = n
            end = time.time()
        return best_node

    # Simulate <times> end game and store stats in Node obj
    def monte_carlo(self, n, TIMES=1000):
        curr_game = n
        while TIMES > 0:
            next_game = random.choice(list(self.gen_children(curr_game)))
            ng = next_game.gen_metagame()
            # Found an end game
            if ng.is_finished():
                if ng.status == 'x':
                    n.win_sim['x'] += 1
                elif ng.status == 'o':
                    n.win_sim['o'] += 1
                TIMES -= 1
                curr_game = n
            # Not an end game
            else:
                curr_game = next_game

    # Use generator for lighter use of memory
    def gen_children(self, n):
        mg = n.gen_metagame()
        # No children if game is finished
        if mg.is_finished():
            return ''
        else:
            # Define what move to play
            # va1:1 == 'x', val:2 == 'o'
            val = 1 if mg.is_x_turn() else 2
            for m in mg.possible_moves():
                yield Node(n, self.gen_entier(n.entier, m, val), n.depth + 1)

    # return entier for new Node
    def gen_entier(self, entier, move, val):
        # Convert everything to binary representation
        e = '{:0169b}'.format(entier)[7:]  # cut 7 first bit of last move
        m = '{:07b}'.format(move)
        v = '{:02b}'.format(val)
        return int((m + e[:move * 2] + v + e[move * 2 + 2:]), base=2)

    # print tree using breadth first traversal, one level per line
    # up to and including a certain depth
    def breadth_first_print(self, d):
        res = [self.root]
        curr_depth = 0
        while res:
            n = res.pop(0)
            new_depth = n.depth
            # Detect change in depth => new level
            if not (curr_depth == new_depth):
                if new_depth > d:
                    return ''
                else:
                    print('')
                    curr_depth = new_depth
            # Print node and add children to res
            print(n, end=' ')
            res += [c for c in self.gen_children(n)]


# Unit tests
if __name__ == '__main__':
    import sys

    # List entiers (start, mid, end) of game
    e_start = [0, 17560855611054130376541150568106588158053314461696,
               420919607789763570242066523263942673343764599209984, 35083889158251552816234256569828680902166599237632,
               327384216624432136456971223113085285114828084543488]
    e_mid = [95051179178743477810187863205886346395614714089472, 416581539391542119814998526403468610719919875903488,
             399043698149532873041539440696619395670940750008320, 77513337958512302519668839161283402032627461738496]
    e_end = [459329034283597291728327479273734123420780266358036, 330716890198477834926403213994701218254008155997460,
             319024877099830611580773735332970962541009996973332, 211971488592955370994408155674042900415300329885696]
    elist = e_start + e_mid + e_end

    # Tests for mode arbre
    if 'p' in sys.argv:
        for i, e in enumerate(elist):
            print('\n\n*****TREE #', i, ', e=', e, '*****\n', sep='')
            gt = Gametree(e)
            # d = randint(0, 15)
            # print("Depth: ", d)
            gt.breadth_first_print(2)  # random depth between 0 - 15

    # Test best_move()
    if 'e' in sys.argv:
        for i, e in enumerate(elist):
            print('\n\n*****TREE #', i, ', e=', e, '*****\n', sep='')
            gt = Gametree(e)
            print(gt.best_move())
