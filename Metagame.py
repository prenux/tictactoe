from Tictactoe import Tictactoe
from Game import Game


class Metagame(Tictactoe):
    def __init__(self, e):
        self.e = e
        self.lm = self.e >> 162 & 127  # lm => last_move
        super().__init__(self.create_games(), ['x', 'o', 'x', ''])

    # returns True if it is x turn
    def is_x_turn(self):
        # True if new game
        if self.e == 0:
            return True
        # True if 'o' played last move
        else:
            return (self.e >> ((80 - self.lm) << 1) & 3) == 2

    # return array of possible squares where to play next
    def possible_moves(self):
        # if Metagame is finished return ''
        if self.is_finished():
            return ''
        # if empty game can play anywhere
        if self.e == 0:
            return list(range(81))
        else:
            ng = self.lm % 9
            next_game = self.squares[ng]
            # if next_game is finished, can play "almost" anywhere
            if next_game.is_finished():
                unfinished = [i for i, g in enumerate(self.squares) if not (g.is_finished())]
                return [i for i in range(81) if (i // 9) in unfinished and not (self.e >> ((80 - i) << 1) & 3)]
            # if next_game is not finished, play only in next_game
            else:
                return [i for i in range(ng * 9, ng * 9 + 9) if not (self.e >> ((80 - i) << 1) & 3)]

    def create_games(self):
        # Construct 9 subgames
        games = []
        squares = []
        for i in range(81):
            val = self.e >> ((80 - i) << 1) & 3
            squares.append(val)
            # Construct a new game every 9 squares
            if len(squares) == 9:
                games.append(Game(squares))
                squares = []
        return games

    def print_ascii(self):
        lines = ['', '', '']
        sep = 29 * '-'
        for i in range(81):
            # Construct symbol, uppercase if last_move
            val = self.e >> ((80 - i) << 1) & 3
            symbol = ' . ' if val == 0 else ' x ' if val == 1 else ' o '
            if i == self.lm:
                symbol = symbol.upper()

            # Add vertical separators between gamecolumns 0-1 && 1-2
            if i % 3 == 2 and (i // 9) % 3 is not 2:
                symbol += '|'

            # Build 3 row at a time ==> gamerow
            lines[(i % 9) // 3] += symbol

            # Print gamerow with horizontal separator between rows 0-1 && 1-2
            if i % 27 == 26:
                for line in lines:
                    print(line)
                if i is not 80:
                    print(sep)
                lines = ['', '', '']  # Unit tests


# Unit testing
if __name__ == "__main__":
    import sys

    # List entiers (start, mid, end) of game
    e_start = [0, 17560855611054130376541150568106588158053314461696,
               420919607789763570242066523263942673343764599209984, 35083889158251552816234256569828680902166599237632,
               327384216624432136456971223113085285114828084543488]
    e_mid = [95051179178743477810187863205886346395614714089472, 416581539391542119814998526403468610719919875903488,
             399043698149532873041539440696619395670940750008320, 77513337958512302519668839161283402032627461738496]
    e_end = [459329034283597291728327479273734123420780266358036, 330716890198477834926403213994701218254008155997460,
             319024877099830611580773735332970962541009996973332, 211971488592955370994408155674042900415300329885696]
    elist = e_start + e_mid + e_end

    # Unit test for mode affichage
    if 'a' in sys.argv:
        for i, e in enumerate(elist):
            # Print board
            print('\n*****METAGAME #', i, ', e=', e, '*****', sep='')
            mg = Metagame(e)
            mg.print_ascii()

            # Print Metagame stats
            print('\n---Metagame stats---')
            print('Status: ', mg.status)
            print('Metagame last_move: ', mg.lm)

            # Sub games stats
            # Verify each individual game status is correct
            print('\n---Subgame stats---')
            for i, game in enumerate(mg.squares):
                print('status', i, ': ' + game.status, '\tlength: ', len(game.squares))

    # Unit test for next_moves
    if 'n' in sys.argv:
        for i, e in enumerate(elist):
            print('\n\n*****METAGAME #', i, ', e=', e, '*****\n', sep='')
            mg = Metagame(e)
            print(mg.possible_moves())
