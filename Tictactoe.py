class Tictactoe:
    def __init__(self, squares, vict_args):
        # 4 possible status : 'd': draw, 'x': x wins, 'o': o wins, '': undecided
        self.victory_check = [{'inc': 1, 'starts': [0, 3, 6]},  # rows
                              {'inc': 3, 'starts': [0, 1, 2]},  # columns
                              {'inc': 4, 'starts': [0]},  # diagonal right -> left
                              {'inc': -2, 'starts': [6]}]  # diagonal left -> right
        self.vict_args = vict_args
        self.squares = squares
        self.status = self.victoire(self.vict_args)

    # True if status is anything but '' (undecided)
    def is_finished(self):
        return bool(self.status)

    def victoire(self, args):
        # Inspire notes cours Marc Feeley (IFT1015 programmation 1)
        for obj in self.victory_check:
            for i in obj['starts']:
                # Proceed only if find 'x' or 'o'
                if self.squares[i] == args[0] or self.squares[i] == args[1]:
                    j = 1
                    while j < 3:
                        if self.squares[i] != self.squares[i + j * obj['inc']]:
                            break
                        j += 1
                    if j == 3:
                        # 'x' or 'o' is victorious
                        return 'x' if self.squares[i] == args[2] else 'o'
        # Check for a draw else return ''
        return 'd' if self.draw(args[3]) else ''

    def draw(self, arg):
        # Game is not draw if there exists an empty square
        for val in self.squares:
            if val == arg:
                return False
        return True
