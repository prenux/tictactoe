"""
TODO:
SI ENTIER == 0 --> debut de partie

0 - 00: case vide
1 - 01: x
2 - 10: o

entier = 169 bits
7 bits derniere case
162 bits: 81 cases * 2 (val de chq case)

cases 0 a 80 (81 cases)
valeur case "i":
valeur = entier >> ((80-i)<<1) & 3
last = entier >> 162 & 127


a faire:
1)(mode defaut)choisir coup optimal selon configuration
$python x.py entier
output:
nouvel entier (decimal)

2)(mode affichage)representer etat actuel partie
$python3 x.py p entier
affiche etat partie sans faire de coup
"x" -> ascii 120
"o" -> ascii 111
"." -> ascii 46
case: chr(32)+chr(120|111|46)+chr(32)
sep. vert: chr(124)
sep. hori: chr(29)
dernier coup: chr(88|79) "X" ou "O" majuscule

3)(mode arbre)produire arber de jeu complet selon profondeur et configuration
argv -> $python3 x.py a profondeur entier
entier: conf depart a partir de laquelle decoule poss coups
chq noeud arbre a comme enfants les conf donnees par chq coup possible
profondeur 2 = racine + 2 gen enfants (3 lignes)
1 niveau par ligne
order enfants pas importance
"""
import sys
from Metagame import Metagame
from Gametree import Gametree
import traceback


def usage():
    print("Usage:")
    print("\tpython3.X tp1_20054572_20037847.py [p] [a profondeur] entier")
    print("\tOption 1: p\t\tMode affichage")
    print("\tOption 2: a profondeur\tMode arbre")


if __name__ == "__main__":
    try:
        params = sys.argv[1:]
        mode = params.pop(0)

        # mode arbre
        if mode == "a":
            depth = int(params.pop(0))
            entier = int(params.pop(0))
            gt = Gametree(entier)
            gt.breadth_first_print(depth)

        # mode affichage
        elif mode == "p":
            entier = int(params.pop(0))
            mg = Metagame(entier)
            mg.print_ascii()

        # mode default
        else:
            entier = int(mode)
            gt = Gametree(entier)
            print(gt.best_move())

    except:
        usage()

    exit()
