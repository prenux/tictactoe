from Metagame import Metagame


class Node:

    def __init__(self, parent, entier, depth):
        self.parent = parent
        self.entier = entier
        self.depth = depth
        self.win_sim = {'x': 0, 'o': 0}

    # Generate metagame only if needed
    # Don't want to store it in memory (too heavy)
    def gen_metagame(self):
        return Metagame(self.entier)

    def __eq__(self, other):
        return self.entier == other.entier

    def __ne__(self, other):
        return not (self.entier == other.entier)

    def __str__(self):
        # need to return str type
        return str(self.entier)
