from Tictactoe import Tictactoe


class Game(Tictactoe):
    def __init__(self, squares):
        self.squares = squares
        super().__init__(squares, [1, 2, 1, 0])

    def __eq__(self, other):
        return self.status == other

    def __ne__(self, other):
        return not (self == other)

    def next_move(self, move):
        pass
